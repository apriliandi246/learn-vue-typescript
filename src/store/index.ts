import { createStore } from "vuex";

export default createStore({
   state: {
      count: 20,
   },
   mutations: {},
   actions: {},
   modules: {},
});
